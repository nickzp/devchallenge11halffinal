package com.example.devchallenge11halffinal.devchallenge11halffinal.labyrinth;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

public class LabyrinthMatrix implements Serializable {
    private int mSizeY;
    private int mSizeX;
    private LabyrinthCell[][] mMatrix;

    public LabyrinthMatrix(int sizeX, int sizeY) {
        this.mSizeY = sizeY;
        this.mSizeX = sizeX;
        createNewMatrix();
    }

    public int getSizeY() {
        return mSizeY;
    }

    public int getSizeX() {
        return mSizeX;
    }

    public LabyrinthCell[][] getMatrix() {
        return mMatrix;
    }

    private void createNewMatrix() {
        mMatrix = new LabyrinthCell[mSizeX][mSizeY];
        resetMatrix();
        LabyrinthPoint startGeneratedPoint = new LabyrinthPoint(0, 0);
        LabyrinthPoint currentGeneratedPoint = startGeneratedPoint;
        ArrayList<LabyrinthCell> notGeneratedCell = new ArrayList<>();
        for (int x = 0; x < mSizeX; x++) {
            for (int y = 0; y < mSizeY; y++) {
                notGeneratedCell.add(mMatrix[x][y]);
            }
        }
        ArrayList<LabyrinthCell> notGeneratedNeighbours;
        ArrayList<LabyrinthPoint> historyRoute = new ArrayList<>();
        do {
            notGeneratedNeighbours = getNotGeneratedNeighbours(currentGeneratedPoint.getX(), currentGeneratedPoint.getY());
            if (!notGeneratedNeighbours.isEmpty()) {
                LabyrinthCell neighbour = notGeneratedNeighbours.get(new Random().nextInt(notGeneratedNeighbours.size()));
                //add to stack
                updateWall(currentGeneratedPoint, neighbour.getLabyrinthPoint(), false);
                LabyrinthCell cell = mMatrix[currentGeneratedPoint.getX()][currentGeneratedPoint.getY()];
                notGeneratedCell.remove(cell);
                cell.setCellGenerated(true);
                historyRoute.add(currentGeneratedPoint);
                mMatrix[currentGeneratedPoint.getX()][currentGeneratedPoint.getY()] = cell;
                currentGeneratedPoint = neighbour.getLabyrinthPoint();
            } else if (!historyRoute.isEmpty()) {
                boolean newCellFind = false;
                LabyrinthCell cell = mMatrix[currentGeneratedPoint.getX()][currentGeneratedPoint.getY()];
                notGeneratedCell.remove(cell);
                cell.setCellGenerated(true);
                mMatrix[currentGeneratedPoint.getX()][currentGeneratedPoint.getY()] = cell;

                for (int historyIndex = historyRoute.size() - 1; historyIndex >= 0; historyIndex--) {
                    LabyrinthPoint hPoint = historyRoute.get(historyIndex);
                    ArrayList<LabyrinthCell> cellsHistory = getNotGeneratedNeighbours(hPoint.getX(), hPoint.getY());
                    if (!cellsHistory.isEmpty()) {
                        currentGeneratedPoint = hPoint;
                        historyRoute.remove(currentGeneratedPoint);
                        newCellFind = true;
                        break;
                    }
                    historyRoute.remove(currentGeneratedPoint);
                }
                if (!newCellFind) {
                    int max = notGeneratedCell.size();
                    if (max > 0) {
                        int value = new Random().nextInt(max);
                        currentGeneratedPoint = notGeneratedCell.get(value).getLabyrinthPoint();
                    } else {
                        break;
                    }
                }
            }
        } while (notGeneratedCell.size() != 0);
    }

    private void resetMatrix() {
        for (int x = 0; x < mSizeX; x++) {
            for (int y = 0; y < mSizeY; y++) {
                mMatrix[x][y] = new LabyrinthCell(true, true, true, true, new LabyrinthPoint(x, y));
            }
        }
    }

    private ArrayList<LabyrinthCell> getNotGeneratedNeighbours(int currentX, int currentY) {
        ArrayList<LabyrinthCell> result = new ArrayList<LabyrinthCell>();
        //checkTopCell
        int topCellY = currentY - 1;
        if (topCellY >= 0) {
            LabyrinthCell topCell = mMatrix[currentX][topCellY];
            if (!topCell.isCellGenerated()) {
                result.add(topCell);
            }
        }
        //checkLeftCell
        int leftCellX = currentX - 1;
        if (leftCellX >= 0) {
            LabyrinthCell topCell = mMatrix[leftCellX][currentY];
            if (!topCell.isCellGenerated()) {
                result.add(topCell);
            }
        }

        //checkRightCell
        int rightCellX = currentX + 1;
        if (rightCellX < mSizeX) {
            LabyrinthCell topCell = mMatrix[rightCellX][currentY];
            if (!topCell.isCellGenerated()) {
                result.add(topCell);
            }
        }
        //checkBottomCell
        int bottomCellY = currentY + 1;
        if (bottomCellY < mSizeY) {
            LabyrinthCell topCell = mMatrix[currentX][bottomCellY];
            if (!topCell.isCellGenerated()) {
                result.add(topCell);
            }
        }
        return result;
    }

    /**
     * @param needWall false - remove wall if true add
     */
    public void updateWall(@NonNull LabyrinthPoint firstLabyrinthPoint, @NonNull LabyrinthPoint secondLabyrinthPoint, boolean needWall) {
        int firstX = firstLabyrinthPoint.getX();
        int firstY = firstLabyrinthPoint.getY();
        int secondX = secondLabyrinthPoint.getX();
        int secondY = secondLabyrinthPoint.getY();
        LabyrinthCell first = mMatrix[firstX][firstY];
        LabyrinthCell second = mMatrix[secondX][secondY];
        if (firstY == secondY) {
            // horizontal
            if (firstX < secondX) {
                // right on first, left on second
                first.setRightBorder(needWall);
                second.setLeftBorder(needWall);
            } else {
                // left on first, rigth on second
                first.setLeftBorder(needWall);
                second.setRightBorder(needWall);
            }
        } else {
            //vertical
            if (firstY > secondY) {
                //top on first, bottom on second
                first.setTopBorder(needWall);
                second.setBottomBorder(needWall);
            } else {
                //bottom on first, top on second
                first.setBottomBorder(needWall);
                second.setTopBorder(needWall);
            }
        }
        mMatrix[firstX][firstY] = first;
        mMatrix[secondX][secondY] = second;
    }
}
