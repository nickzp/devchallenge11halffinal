package com.example.devchallenge11halffinal.devchallenge11halffinal.views;


import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

import com.example.devchallenge11halffinal.devchallenge11halffinal.enums.LabyrinthCellWallsType;
import com.example.devchallenge11halffinal.devchallenge11halffinal.labyrinth.LabyrinthCell;
import com.example.devchallenge11halffinal.devchallenge11halffinal.labyrinth.LabyrinthGameListener;
import com.example.devchallenge11halffinal.devchallenge11halffinal.labyrinth.LabyrinthMatrix;
import com.example.devchallenge11halffinal.devchallenge11halffinal.labyrinth.LabyrinthPoint;
import com.example.devchallenge11halffinal.devchallenge11halffinal.listeners.BallAnimationListener;
import com.example.devchallenge11halffinal.devchallenge11halffinal.listeners.MoveToHoleListener;

public class LabyrinthView extends View {
    public final static int CELL_SIZE = 50;
    public final static int CELL_HALF_SIZE = 25;
    public final static int HOLE_SIZE = 23;
    private final static int ANIMATION_DURATION = 500;
    private static boolean isAnimationInProgress;
    private BallAnimationListener mBallAnimationListener;
    private LabyrinthGameListener mLabyrinthGameListener;
    private LabyrinthMatrix mLabyrinthMatrix;
    private LabyrinthPoint mHolePoint;
    private View mBallView;
    private LabyrinthPoint mBallPixelPoint;
    private int mHoleX;
    private int mHoleY;
    private LabyrinthMatrix mMatrixWithoutDeadEnd;
    private MoveToHoleListener mMoveToHoleListener;

    public LabyrinthView(Context context) {
        super(context);
    }

    public LabyrinthView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public LabyrinthView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public LabyrinthView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public LabyrinthPoint getHolePoint() {
        return mHolePoint;
    }

    public void setHolePoint(@NonNull LabyrinthPoint holePoint) {
        this.mHolePoint = holePoint;
    }

    public LabyrinthPoint getBallPoint() {
        return mBallPixelPoint;
    }

    public void setLabyrinthMatrix(@NonNull LabyrinthMatrix labyrinthMatrix) {
        this.mLabyrinthMatrix = labyrinthMatrix;
    }

    public void setLabyrinthGameListener(@NonNull LabyrinthGameListener labyrinthGameListener) {
        this.mLabyrinthGameListener = labyrinthGameListener;
    }

    public void setBallMoveListener(@NonNull BallAnimationListener ballAnimationListener) {
        this.mBallAnimationListener = ballAnimationListener;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (mLabyrinthMatrix == null) {
            return;
        }

        Paint paint = new Paint();
        paint.setColor(Color.BLUE);

        for (int x = 0; x < mLabyrinthMatrix.getSizeX(); x++) {
            for (int y = 0; y < mLabyrinthMatrix.getSizeY(); y++) {
                LabyrinthCell cell = mLabyrinthMatrix.getMatrix()[x][y];
                int xLeft = x * CELL_SIZE;
                int yTop = y * CELL_SIZE;
                int xRight = xLeft + CELL_SIZE;
                int yBottom = yTop + CELL_SIZE;
                if (cell.isTopBorder()) {
                    canvas.drawLine(xLeft, yTop, xRight, yTop, paint);
                }
                if (cell.isLeftBorder()) {
                    canvas.drawLine(xLeft, yTop, xLeft, yBottom, paint);
                }
                if (cell.isRightBorder()) {
                    canvas.drawLine(xRight, yTop, xRight, yBottom, paint);
                }
                if (cell.isBottomBorder()) {
                    canvas.drawLine(xLeft, yBottom, xRight, yBottom, paint);
                }
            }
        }

        Paint paintHole = new Paint();
        paintHole.setColor(Color.GRAY);
        mHoleX = mHolePoint.getX() * CELL_SIZE + CELL_HALF_SIZE;
        mHoleY = mHolePoint.getY() * CELL_SIZE + CELL_HALF_SIZE;
        canvas.drawCircle(mHoleX, mHoleY, HOLE_SIZE, paintHole);
    }

    public void setBallView(@NonNull View view, @NonNull LabyrinthPoint ballPoint) {
        isAnimationInProgress = false;
        mBallView = view;
        mBallPixelPoint = ballPoint;
        TranslateAnimation animation = new TranslateAnimation(0, ballPoint.getX() * CELL_SIZE, 0, ballPoint.getY() * CELL_SIZE);
        animation.setFillAfter(true);
        mBallView.startAnimation(animation);

    }

    public void moveBottom() {
        if (!getCellByLocation(mBallPixelPoint.getX(), mBallPixelPoint.getY()).isBottomBorder()) {
            runAnimation(mBallPixelPoint.getX(), mBallPixelPoint.getY() + 1);
        }
    }

    public void moveUp() {
        if (!getCellByLocation(mBallPixelPoint.getX(), mBallPixelPoint.getY()).isTopBorder())
            runAnimation(mBallPixelPoint.getX(), mBallPixelPoint.getY() - 1);
    }

    public void moveLeft() {
        if (!getCellByLocation(mBallPixelPoint.getX(), mBallPixelPoint.getY()).isLeftBorder()) {
            runAnimation(mBallPixelPoint.getX() - 1, mBallPixelPoint.getY());
        }
    }

    public void moveRight() {
        if (!getCellByLocation(mBallPixelPoint.getX(), mBallPixelPoint.getY()).isRightBorder()) {
            runAnimation(mBallPixelPoint.getX() + 1, mBallPixelPoint.getY());
        }
    }

    private void runAnimation(final int newX, final int newY) {
        TranslateAnimation animation = new TranslateAnimation(mBallPixelPoint.getX() * CELL_SIZE, newX * CELL_SIZE, mBallPixelPoint.getY() * CELL_SIZE, newY * CELL_SIZE);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                isAnimationInProgress = true;
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mBallPixelPoint.setY(newY);
                mBallPixelPoint.setX(newX);
                isAnimationInProgress = false;
                if (mBallAnimationListener != null) {
                    mBallAnimationListener.moveAnimationEnd();
                }

                if (mBallPixelPoint.equals(mHolePoint)) {
                    mLabyrinthGameListener.gameOver();
                    if (mMoveToHoleListener != null) {
                        mMoveToHoleListener = null;
                    }
                } else if (mMoveToHoleListener != null) {
                    mMoveToHoleListener.readyForNextMove();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        animation.setDuration(ANIMATION_DURATION);
        animation.setFillAfter(true);
        if (!isAnimationInProgress) {
            mBallView.startAnimation(animation);
        }
    }

    private LabyrinthCell getCellByLocation(int xPixels, int yPixels) {
        return mLabyrinthMatrix.getMatrix()[xPixels][yPixels];
    }

    public void moveToHole() {
        moveMatrixWithoutDeadEnd(getMatrixWithoutDeadEnd());
    }

    private LabyrinthMatrix getMatrixWithoutDeadEnd() {
        LabyrinthMatrix toHoleMatrix = mLabyrinthMatrix;
        int totalCellCount = toHoleMatrix.getSizeX() * toHoleMatrix.getSizeY();
        for (int i = 0; i < totalCellCount; i++) {
            for (int x = 0; x < toHoleMatrix.getSizeX(); x++) {
                for (int y = 0; y < toHoleMatrix.getSizeY(); y++) {
                    LabyrinthCell cell = toHoleMatrix.getMatrix()[x][y];
                    if (cell.isCloseCell()) {
                        continue;
                    }
                    LabyrinthPoint point = new LabyrinthPoint(x, y);
                    if (point.equals(mHolePoint) || point.equals(mBallPixelPoint)) {
                        cell.setCloseCell(true);
                        continue;
                    }

                    @LabyrinthCellWallsType.Value
                    int walls = cell.getWalls();
                    if (walls == LabyrinthCellWallsType.MORE_THAN_ONE || walls == LabyrinthCellWallsType.ALL_WALLS) {
                        continue;
                    }

                    toHoleMatrix.updateWall(point, getOneWallNeighbor(x, y, walls), true);
                    cell.setCloseCell(true);
                }
            }
        }
        return toHoleMatrix;
    }

    private LabyrinthPoint getOneWallNeighbor(int x, int y, @LabyrinthCellWallsType.Value int walls) {
        LabyrinthPoint neighborPoint = new LabyrinthPoint(x, y);
        if (walls == LabyrinthCellWallsType.ONLY_BOTTOM) {
            neighborPoint.setY(y + 1);
        } else if (walls == LabyrinthCellWallsType.ONLY_LEFT) {
            neighborPoint.setX(x - 1);
        } else if (walls == LabyrinthCellWallsType.ONLY_RIGHT) {
            neighborPoint.setX(x + 1);
        } else if (walls == LabyrinthCellWallsType.ONLY_TOP) {
            neighborPoint.setY(y - 1);
        }
        return neighborPoint;
    }

    private void moveMatrixWithoutDeadEnd(@NonNull LabyrinthMatrix labyrinthMatrix) {
        mMatrixWithoutDeadEnd = labyrinthMatrix;
        mMoveToHoleListener = new MoveToHoleListener() {
            @Override
            public void readyForNextMove() {
                moveOneStepToHole();
            }
        };
        moveOneStepToHole();
    }

    private void moveOneStepToHole() {
        @LabyrinthCellWallsType.Value
        int x = mBallPixelPoint.getX();
        int y = mBallPixelPoint.getY();

        int walls = mMatrixWithoutDeadEnd.getMatrix()[x][y].getWalls();
        if (walls == LabyrinthCellWallsType.ONLY_BOTTOM) {
            moveBottom();
        } else if (walls == LabyrinthCellWallsType.ONLY_LEFT) {
            moveLeft();
        } else if (walls == LabyrinthCellWallsType.ONLY_RIGHT) {
            moveRight();
        } else if (walls == LabyrinthCellWallsType.ONLY_TOP) {
            moveUp();
        }
        mMatrixWithoutDeadEnd.updateWall(mBallPixelPoint, getOneWallNeighbor(x, y, walls), true);
    }
}
