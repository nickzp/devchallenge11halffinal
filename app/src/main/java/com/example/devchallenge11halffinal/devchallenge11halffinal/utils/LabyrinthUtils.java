package com.example.devchallenge11halffinal.devchallenge11halffinal.utils;

import android.support.annotation.NonNull;

import com.example.devchallenge11halffinal.devchallenge11halffinal.enums.GameModeType;
import com.example.devchallenge11halffinal.devchallenge11halffinal.labyrinth.LabyrinthMatrix;
import com.example.devchallenge11halffinal.devchallenge11halffinal.labyrinth.LabyrinthPoint;
import com.example.devchallenge11halffinal.devchallenge11halffinal.storage.preferences.AppPreferences;

import java.util.Random;

public class LabyrinthUtils {
    /**
     * @return location for hole. if game in normal mode return leftBottom location
     */
    public static LabyrinthPoint getHolePoint(@NonNull LabyrinthMatrix mazeMatrix, @NonNull AppPreferences appPreferences) {
        if (appPreferences.getGameModeType() == GameModeType.NORMAL) {
            return new LabyrinthPoint(mazeMatrix.getSizeX() - 1, mazeMatrix.getSizeY() - 1);
        }
        return new LabyrinthPoint(new Random().nextInt(mazeMatrix.getSizeX()), new Random().nextInt(mazeMatrix.getSizeY()));
    }

    public static LabyrinthPoint getBallPoint(@NonNull LabyrinthMatrix mazeMatrix, @NonNull AppPreferences appPreferences, @NonNull LabyrinthPoint holePoint) {
        if (appPreferences.getGameModeType() == GameModeType.NORMAL) {
            return new LabyrinthPoint(0, 0);
        }
        for (; ; ) {
            LabyrinthPoint getBallPoint = new LabyrinthPoint(new Random().nextInt(mazeMatrix.getSizeX()), new Random().nextInt(mazeMatrix.getSizeY()));
            if (!holePoint.equals(getBallPoint)) {
                return getBallPoint;
            }
        }
    }
}
