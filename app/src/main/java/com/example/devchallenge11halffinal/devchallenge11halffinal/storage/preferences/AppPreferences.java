package com.example.devchallenge11halffinal.devchallenge11halffinal.storage.preferences;


import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.example.devchallenge11halffinal.devchallenge11halffinal.enums.GameModeType;
import com.example.devchallenge11halffinal.devchallenge11halffinal.enums.LabyrinthPointType;
import com.example.devchallenge11halffinal.devchallenge11halffinal.labyrinth.LabyrinthCalculation;
import com.example.devchallenge11halffinal.devchallenge11halffinal.labyrinth.LabyrinthPoint;
import com.example.devchallenge11halffinal.devchallenge11halffinal.sensors.SensorHelper;

public class AppPreferences {
    private static final String PREFERENCES_NAME = "devchallenge11halffinal_preferences";
    private static final String CELL_COUNT_Y_KEY = "CELL_COUNT_Y_KEY";
    private static final String CELL_COUNT_X_KEY = "CELL_COUNT_X_KEY";
    private static final String GAME_MODE_TYPE_KEY = "GAME_MODE_TYPE_KEY";
    private static final String FILTER_ANGLE_Y_KEY = "FILTER_ANGLE_Y_KEY";
    private static final String FILTER_ANGLE_X_KEY = "FILTER_ANGLE_X_KEY";
    private static final String POINT_X_PART_KEY = "POINT_X_PART_KEY";
    private static final String POINT_Y_PART_KEY = "POINT_Y_PART_KEY";

    private Context mContext;

    public AppPreferences(@NonNull Context context) {
        mContext = context;
    }

    private SharedPreferences getSharedPreferences() {
        return mContext.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    private SharedPreferences.Editor getPreferencesEditor() {
        return mContext.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE).edit();
    }

    public int getCellCountX() {
        return getSharedPreferences().getInt(CELL_COUNT_X_KEY, LabyrinthCalculation.DEFAULT_CELL_COUNT);
    }

    public void setCellCountX(int cellCountX) {
        SharedPreferences.Editor editor = getPreferencesEditor();
        editor.putInt(CELL_COUNT_X_KEY, cellCountX);
        editor.commit();
    }

    public int getCellCountY() {
        return getSharedPreferences().getInt(CELL_COUNT_Y_KEY, LabyrinthCalculation.DEFAULT_CELL_COUNT);
    }

    public void setCellCountY(int cellCountY) {
        SharedPreferences.Editor editor = getPreferencesEditor();
        editor.putInt(CELL_COUNT_Y_KEY, cellCountY);
        editor.commit();
    }

    @GameModeType.Value
    public int getGameModeType() {
        //noinspection WrongConstant
        return getSharedPreferences().getInt(GAME_MODE_TYPE_KEY, GameModeType.NORMAL);
    }

    public void setGameModeType(@GameModeType.Value int gameModeType) {
        SharedPreferences.Editor editor = getPreferencesEditor();
        editor.putInt(GAME_MODE_TYPE_KEY, gameModeType);
        editor.commit();
    }

    public float getFilterAngleX() {
        //noinspection WrongConstant
        return getSharedPreferences().getFloat(FILTER_ANGLE_X_KEY, SensorHelper.FILTER_ANGLE);
    }

    public void setFilterAngleX(float anglex) {
        SharedPreferences.Editor editor = getPreferencesEditor();
        editor.putFloat(FILTER_ANGLE_X_KEY, anglex);
        editor.commit();
    }

    public float getFilterAngleY() {
        //noinspection WrongConstant
        return getSharedPreferences().getFloat(FILTER_ANGLE_Y_KEY, SensorHelper.FILTER_ANGLE);
    }

    public void setFilterAngleY(float angleY) {
        SharedPreferences.Editor editor = getPreferencesEditor();
        editor.putFloat(FILTER_ANGLE_X_KEY, angleY);
        editor.commit();
    }

    public void setPoint(@NonNull String fileGameName, @NonNull LabyrinthPoint labyrinthPoint, @LabyrinthPointType.Value int labyrinthPointType) {
        SharedPreferences.Editor editor = getPreferencesEditor();
        editor.putInt(fileGameName + labyrinthPointType + POINT_X_PART_KEY, labyrinthPoint.getX());
        editor.putInt(fileGameName + labyrinthPointType + POINT_Y_PART_KEY, labyrinthPoint.getY());
        editor.commit();
    }

    public LabyrinthPoint getPoint(@NonNull String fileGameName, @LabyrinthPointType.Value int labyrinthPointType) {
        return new LabyrinthPoint(
                getSharedPreferences().getInt(fileGameName + labyrinthPointType + POINT_X_PART_KEY, 0),
                getSharedPreferences().getInt(fileGameName + labyrinthPointType + POINT_Y_PART_KEY, 0));
    }
}
