package com.example.devchallenge11halffinal.devchallenge11halffinal.storage;


import android.content.Context;
import android.support.annotation.NonNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

public class SaveToFile {
    public static String FILE_PREFIX = "devchallenge11halffinal";

    public static String write(@NonNull Context context, @NonNull Object savingObject, @NonNull String fileName) {
        ObjectOutput out;
        try {
            out = new ObjectOutputStream(new FileOutputStream(new File(context.getCacheDir(), "") + File.separator + FILE_PREFIX + fileName));
            out.writeObject(savingObject);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return FILE_PREFIX + fileName;
    }
}
