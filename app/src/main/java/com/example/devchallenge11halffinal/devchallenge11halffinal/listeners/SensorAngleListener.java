package com.example.devchallenge11halffinal.devchallenge11halffinal.listeners;


public interface SensorAngleListener {
    void newSensorAngel(float xAngle, float yAngle);
}

