package com.example.devchallenge11halffinal.devchallenge11halffinal.listeners;

import android.support.annotation.NonNull;

import java.io.File;

public interface SavedListFileClick {
    void selectFile(@NonNull File file);
}
