package com.example.devchallenge11halffinal.devchallenge11halffinal.labyrinth;

public interface LabyrinthGameListener {
    void gameOver();
}
