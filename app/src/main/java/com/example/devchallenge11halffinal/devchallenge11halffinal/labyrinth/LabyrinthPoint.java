package com.example.devchallenge11halffinal.devchallenge11halffinal.labyrinth;


import java.io.Serializable;

public class LabyrinthPoint implements Serializable {
    private int mX;
    private int mY;

    public LabyrinthPoint(int x, int y) {
        this.mX = x;
        this.mY = y;
    }

    public int getX() {
        return mX;
    }

    public void setX(int x) {
        this.mX = x;
    }

    public int getY() {
        return mY;
    }

    public void setY(int y) {
        this.mY = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LabyrinthPoint that = (LabyrinthPoint) o;
        return mX == that.mX && mY == that.mY;
    }

    @Override
    public int hashCode() {
        int result = mX;
        result = 31 * result + mY;
        return result;
    }
}
