package com.example.devchallenge11halffinal.devchallenge11halffinal.listeners;

public interface SensorListener {

    void rotatedToTop();

    void rotatedToBottom();

    void rotatedToLeft();

    void rotatedToRight();

}
