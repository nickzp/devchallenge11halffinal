package com.example.devchallenge11halffinal.devchallenge11halffinal.utils;


import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {
    public static String returnNow() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy_HHmmss");
        return sdf.format(new Date());
    }
}
