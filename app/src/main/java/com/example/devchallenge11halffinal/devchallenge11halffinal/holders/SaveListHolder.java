package com.example.devchallenge11halffinal.devchallenge11halffinal.holders;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;

import com.example.devchallenge11halffinal.devchallenge11halffinal.R;

public class SaveListHolder extends ViewHolder {
    private TextView mGameNameTextView;
    private View mRootView;

    public SaveListHolder(View itemView) {
        super(itemView);
        mRootView = itemView;
        mGameNameTextView = (TextView) itemView.findViewById(R.id.gameName);

    }

    public View getRootView() {
        return mRootView;
    }

    public void setGameName(String gameName) {
        this.mGameNameTextView.setText(gameName);
    }
}
