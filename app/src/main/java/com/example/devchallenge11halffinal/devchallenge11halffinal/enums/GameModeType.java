package com.example.devchallenge11halffinal.devchallenge11halffinal.enums;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class GameModeType {
    public static final int NORMAL = 0;
    public static final int RANDOM = 1;

    @IntDef({NORMAL, RANDOM})

    @Retention(RetentionPolicy.SOURCE)
    public @interface Value {
    }
}
