package com.example.devchallenge11halffinal.devchallenge11halffinal.labyrinth;


import com.example.devchallenge11halffinal.devchallenge11halffinal.enums.LabyrinthCellWallsType;

import java.io.Serializable;

public class LabyrinthCell implements Serializable {
    private boolean isTopBorder;
    private boolean isLeftBorder;
    private boolean isRightBorder;
    private boolean isBottomBorder;
    private boolean isCloseCell;
    private boolean isCellGenerated;
    private LabyrinthPoint mLabyrinthPoint;

    public LabyrinthCell(boolean isTopBorder, boolean isLeftBorder, boolean isRightBorder, boolean isBottomBorder, LabyrinthPoint labyrinthPoint) {
        this.isTopBorder = isTopBorder;
        this.isLeftBorder = isLeftBorder;
        this.isRightBorder = isRightBorder;
        this.isBottomBorder = isBottomBorder;
        this.mLabyrinthPoint = labyrinthPoint;
    }

    public boolean isCellGenerated() {
        return isCellGenerated;
    }

    public void setCellGenerated(boolean cellGenerated) {
        isCellGenerated = cellGenerated;
    }

    public LabyrinthPoint getLabyrinthPoint() {
        return mLabyrinthPoint;
    }

    public void setLabyrinthPoint(LabyrinthPoint labyrinthPoint) {
        this.mLabyrinthPoint = labyrinthPoint;
    }

    public boolean isTopBorder() {
        return isTopBorder;
    }

    public void setTopBorder(boolean topBorder) {
        isTopBorder = topBorder;
    }

    public boolean isLeftBorder() {
        return isLeftBorder;
    }

    public void setLeftBorder(boolean leftBorder) {
        isLeftBorder = leftBorder;
    }

    public boolean isRightBorder() {
        return isRightBorder;
    }

    public void setRightBorder(boolean rightBorder) {
        isRightBorder = rightBorder;
    }

    public boolean isBottomBorder() {
        return isBottomBorder;
    }

    public void setBottomBorder(boolean bottomBorder) {
        isBottomBorder = bottomBorder;
    }

    public boolean isCloseCell() {
        return isCloseCell;
    }

    public void setCloseCell(boolean isCloseCell) {
        this.isCloseCell = isCloseCell;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LabyrinthCell that = (LabyrinthCell) o;

        if (isTopBorder != that.isTopBorder) return false;
        if (isLeftBorder != that.isLeftBorder) return false;
        if (isRightBorder != that.isRightBorder) return false;
        if (isBottomBorder != that.isBottomBorder) return false;
        if (isCloseCell != that.isCloseCell) return false;
        if (isCellGenerated != that.isCellGenerated) return false;
        return mLabyrinthPoint != null ? mLabyrinthPoint.equals(that.mLabyrinthPoint) : that.mLabyrinthPoint == null;

    }

    @Override
    public int hashCode() {
        int result = (isTopBorder ? 1 : 0);
        result = 31 * result + (isLeftBorder ? 1 : 0);
        result = 31 * result + (isRightBorder ? 1 : 0);
        result = 31 * result + (isBottomBorder ? 1 : 0);
        result = 31 * result + (isCloseCell ? 1 : 0);
        result = 31 * result + (isCellGenerated ? 1 : 0);
        result = 31 * result + (mLabyrinthPoint != null ? mLabyrinthPoint.hashCode() : 0);
        return result;
    }

    @LabyrinthCellWallsType.Value
    public int getWalls() {
        if (!isTopBorder && isBottomBorder && isLeftBorder && isRightBorder) {
            return LabyrinthCellWallsType.ONLY_TOP;
        }
        if (isTopBorder && !isBottomBorder && isLeftBorder && isRightBorder) {
            return LabyrinthCellWallsType.ONLY_BOTTOM;
        }
        if (isTopBorder && isBottomBorder && !isLeftBorder && isRightBorder) {
            return LabyrinthCellWallsType.ONLY_LEFT;
        }
        if (isTopBorder && isBottomBorder && isLeftBorder && !isRightBorder) {
            return LabyrinthCellWallsType.ONLY_RIGHT;
        }
        if (!isTopBorder || !isBottomBorder || !isLeftBorder || !isRightBorder) {
            return LabyrinthCellWallsType.MORE_THAN_ONE;
        }
        return LabyrinthCellWallsType.ALL_WALLS;
    }
}
