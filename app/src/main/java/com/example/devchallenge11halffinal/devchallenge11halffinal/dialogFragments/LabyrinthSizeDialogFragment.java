package com.example.devchallenge11halffinal.devchallenge11halffinal.dialogFragments;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.devchallenge11halffinal.devchallenge11halffinal.R;
import com.example.devchallenge11halffinal.devchallenge11halffinal.labyrinth.LabyrinthCalculation;
import com.example.devchallenge11halffinal.devchallenge11halffinal.listeners.DialogButtonListener;
import com.example.devchallenge11halffinal.devchallenge11halffinal.storage.preferences.AppPreferences;
import com.example.devchallenge11halffinal.devchallenge11halffinal.utils.DisplayUtils;

public class LabyrinthSizeDialogFragment extends DialogFragment {
    private LabyrinthCalculation mLabyrinthCalculation;
    private AppPreferences mAppPreferences;
    private String mErrorInputString;
    private DialogButtonListener mDialogButtonListener;

    public void setDialogButtonListener(@NonNull DialogButtonListener dialogButtonListener) {
        mDialogButtonListener = dialogButtonListener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        mAppPreferences = new AppPreferences(getActivity());
        View dialogView = getActivity().getLayoutInflater().inflate(R.layout.labyrinth_size_dialog_fragment, null);

        TextView textView = (TextView) dialogView.findViewById(R.id.textView);
        final EditText sizeXEditText = (EditText) dialogView.findViewById(R.id.size_x);
        sizeXEditText.setText(String.valueOf(mAppPreferences.getCellCountX()));
        sizeXEditText.setSelection(sizeXEditText.getText().length());
        final EditText sizeYEditText = (EditText) dialogView.findViewById(R.id.size_y);
        sizeYEditText.setText(String.valueOf(mAppPreferences.getCellCountY()));
        sizeYEditText.setSelection(sizeYEditText.getText().length());

        String info = getResources().getString(R.string.labyrinth_size_params);
        mLabyrinthCalculation = new LabyrinthCalculation(DisplayUtils.getWidthInPixels(getActivity()));
        textView.setText(String.format(info, LabyrinthCalculation.MIN_CELL_COUNT,
                mLabyrinthCalculation.countCellCorrector(LabyrinthCalculation.MAX_CELL_COUNT)));
        mErrorInputString = String.format(getResources().getString(R.string.labyrinth_size_error),
                LabyrinthCalculation.MIN_CELL_COUNT,
                mLabyrinthCalculation.countCellCorrector(LabyrinthCalculation.MAX_CELL_COUNT));

        dialogBuilder.setView(dialogView);
        dialogBuilder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mAppPreferences.setCellCountX(Integer.parseInt(sizeXEditText.getText().toString()));
                mAppPreferences.setCellCountY(Integer.parseInt(sizeYEditText.getText().toString()));
                if (mDialogButtonListener != null) {
                    mDialogButtonListener.OnPositiveButtonClick();
                }

            }
        });
        dialogBuilder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        final AlertDialog dialog = dialogBuilder.create();
        sizeXEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int newXSize = 0;
                boolean parseIntOk = false;
                try {
                    newXSize = Integer.parseInt(sizeXEditText.getText().toString());
                    parseIntOk = true;
                } catch (NumberFormatException ignored) {

                }
                if (parseIntOk && newXSize == mLabyrinthCalculation.countCellCorrector(newXSize)) {
                    sizeXEditText.setError(null);
                    if (sizeYEditText.getError() == null) {
                        dialog.getButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                    }
                } else {
                    sizeXEditText.setError(mErrorInputString);
                    dialog.getButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        sizeYEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int newYSize = 0;
                boolean parseIntOk = false;
                try {
                    newYSize = Integer.parseInt(sizeYEditText.getText().toString());
                    parseIntOk = true;
                } catch (NumberFormatException ignored) {

                }
                if (parseIntOk && newYSize == mLabyrinthCalculation.countCellCorrector(newYSize)) {
                    sizeYEditText.setError(null);
                    if (sizeXEditText.getError() == null) {
                        dialog.getButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE).setEnabled(true);
                    }
                } else {
                    sizeYEditText.setError(mErrorInputString);
                    dialog.getButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE).setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return dialog;
    }
}
