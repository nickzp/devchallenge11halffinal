package com.example.devchallenge11halffinal.devchallenge11halffinal.labyrinth;

import com.example.devchallenge11halffinal.devchallenge11halffinal.views.LabyrinthView;

public class LabyrinthCalculation {
    public final static int MIN_CELL_COUNT = 2;
    public final static int DEFAULT_CELL_COUNT = 5;
    public final static int MAX_CELL_COUNT = 20;
    private int mMaxDeviceCellCount;

    public LabyrinthCalculation(int screenWidth) {
        mMaxDeviceCellCount = screenWidth / LabyrinthView.CELL_SIZE;
    }

    public int countCellCorrector(int countCell) {
        if (countCell < MIN_CELL_COUNT) {
            return DEFAULT_CELL_COUNT;
        }
        if (countCell >= MAX_CELL_COUNT || countCell >= mMaxDeviceCellCount) {
            return mMaxDeviceCellCount > MAX_CELL_COUNT ? MAX_CELL_COUNT : mMaxDeviceCellCount;
        }
        return countCell;
    }
}
