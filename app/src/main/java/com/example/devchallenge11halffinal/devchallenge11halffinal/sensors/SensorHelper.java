package com.example.devchallenge11halffinal.devchallenge11halffinal.sensors;


import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.support.annotation.NonNull;
import android.view.Surface;

import com.example.devchallenge11halffinal.devchallenge11halffinal.listeners.SensorAngleListener;
import com.example.devchallenge11halffinal.devchallenge11halffinal.listeners.SensorListener;

public class SensorHelper {
    public final static int FILTER_ANGLE = 20;
    private final static int MAGNETIC_ARRAY_SIZE = 3;
    private final static int ACCELEROMETER_ARRAY_SIZE = 3;

    private float[] mAccelerometerValues = new float[ACCELEROMETER_ARRAY_SIZE];
    private float[] mMagneticValues = new float[MAGNETIC_ARRAY_SIZE];
    private float[] mDeviceAngle = new float[3];
    private float[] mInRotationMatrix = new float[9];
    private float[] mOutRotationMatrix = new float[9];
    private float[] mRotationMatrix = new float[9];
    private SensorListener mSensorListener;
    private SensorAngleListener mSensorAngleListener;
    private int mRotation;
    private float mAngleX;
    private float mAngleY;

    public void setSensorListener(@NonNull SensorListener sensorListener) {
        mSensorListener = sensorListener;
    }

    public void setSensorAngleListener(@NonNull SensorAngleListener sensorAngleListener) {
        mSensorAngleListener = sensorAngleListener;
    }

    public void setRotation(int rotation) {
        this.mRotation = rotation;
    }

    public void setFilterXAngle(float angleX) {
        mAngleX = Math.abs(angleX);
    }

    public void setFilterYAngle(float angleY) {
        mAngleY = Math.abs(angleY);
    }

    public void callEvents() {

        if (mSensorAngleListener != null) {
            mSensorAngleListener.newSensorAngel(mDeviceAngle[2], mDeviceAngle[1]);
        }
        if (mSensorListener != null) {
            if (mDeviceAngle[1] > mAngleY) {
                mSensorListener.rotatedToTop();
                return;
            }
            if (mDeviceAngle[1] <= -1 * mAngleY) {
                mSensorListener.rotatedToBottom();
                return;
            }
            if (mDeviceAngle[2] >= mAngleX) {
                mSensorListener.rotatedToRight();
                return;
            }
            if (mDeviceAngle[2] <= -1 * mAngleX) {
                mSensorListener.rotatedToLeft();
                return;
            }
        }

    }

    public void getDeviceOrientation() {
        SensorManager.getRotationMatrix(mRotationMatrix, null, mAccelerometerValues, mMagneticValues);
    }

    public void getActualDeviceOrientation() {
        SensorManager.getRotationMatrix(mInRotationMatrix, null, mAccelerometerValues, mMagneticValues);
        int xAxis = SensorManager.AXIS_X;
        int yAxis = SensorManager.AXIS_Y;
        switch (mRotation) {
            case (Surface.ROTATION_0):
                break;
            case (Surface.ROTATION_90):
                xAxis = SensorManager.AXIS_Y;
                yAxis = SensorManager.AXIS_MINUS_X;
                break;
            case (Surface.ROTATION_180):
                yAxis = SensorManager.AXIS_MINUS_Y;
                break;
            case (Surface.ROTATION_270):
                xAxis = SensorManager.AXIS_MINUS_Y;
                yAxis = SensorManager.AXIS_X;
                break;
            default:
                break;
        }
        SensorManager.remapCoordinateSystem(mInRotationMatrix, xAxis, yAxis, mOutRotationMatrix);
        SensorManager.getOrientation(mOutRotationMatrix, mDeviceAngle);
        mDeviceAngle[0] = (float) Math.toDegrees(mDeviceAngle[0]);
        mDeviceAngle[1] = (float) Math.toDegrees(mDeviceAngle[1]);
        mDeviceAngle[2] = (float) Math.toDegrees(mDeviceAngle[2]);
    }

    public void accelerometerSensorChanged(SensorEvent event) {
        System.arraycopy(event.values, 0, mAccelerometerValues, 0, ACCELEROMETER_ARRAY_SIZE);
    }

    public void magneticFieldSensorChanged(SensorEvent event) {
        System.arraycopy(event.values, 0, mMagneticValues, 0, MAGNETIC_ARRAY_SIZE);
    }
}
