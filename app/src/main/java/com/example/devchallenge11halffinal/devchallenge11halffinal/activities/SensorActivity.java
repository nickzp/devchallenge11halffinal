package com.example.devchallenge11halffinal.devchallenge11halffinal.activities;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.devchallenge11halffinal.devchallenge11halffinal.listeners.SensorAngleListener;
import com.example.devchallenge11halffinal.devchallenge11halffinal.listeners.SensorListener;
import com.example.devchallenge11halffinal.devchallenge11halffinal.sensors.SensorHelper;
import com.example.devchallenge11halffinal.devchallenge11halffinal.storage.preferences.AppPreferences;
import com.example.devchallenge11halffinal.devchallenge11halffinal.utils.DisplayUtils;

import java.util.Timer;
import java.util.TimerTask;

public class SensorActivity extends AppCompatActivity {
    private final static int TIME_SENSOR_UPDATE = 100;
    private SensorManager mSensorManager;
    private Sensor mSensorAccel;
    private Sensor mSensorMagnet;
    private Timer mTimer;
    private SensorHelper mSensorHelper;
    SensorEventListener listener = new SensorEventListener() {
        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            switch (event.sensor.getType()) {
                case Sensor.TYPE_ACCELEROMETER:
                    mSensorHelper.accelerometerSensorChanged(event);
                    break;
                case Sensor.TYPE_MAGNETIC_FIELD:
                    mSensorHelper.magneticFieldSensorChanged(event);
                    break;
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSensorHelper = new SensorHelper();
        AppPreferences appPreferences = new AppPreferences(this);
        mSensorHelper.setFilterXAngle(appPreferences.getFilterAngleX());
        mSensorHelper.setFilterYAngle(appPreferences.getFilterAngleY());
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensorAccel = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorMagnet = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
    }

    public void setSensorListener(@NonNull SensorListener sensorListener) {
        if (mSensorHelper != null) {
            mSensorHelper.setSensorListener(sensorListener);
        }
    }

    public void setSensorAngleListener(@NonNull SensorAngleListener sensorAngleListener) {
        if (mSensorHelper != null) {
            mSensorHelper.setSensorAngleListener(sensorAngleListener);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(listener, mSensorAccel, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(listener, mSensorMagnet, SensorManager.SENSOR_DELAY_NORMAL);
        resetTimer();
        mSensorHelper.setRotation(DisplayUtils.getRotation(this));
    }

    protected void resetTimer
            () {
        if (mTimer != null) {
            mTimer.cancel();
            mTimer.purge();
        }
        mTimer = new Timer();
        TimerTask mTimerTask = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mSensorHelper.getDeviceOrientation();
                        mSensorHelper.getActualDeviceOrientation();
                        mSensorHelper.callEvents();
                    }
                });
            }
        };
        mTimer.schedule(mTimerTask, 0, TIME_SENSOR_UPDATE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(listener);
        mTimer.cancel();
    }
}
