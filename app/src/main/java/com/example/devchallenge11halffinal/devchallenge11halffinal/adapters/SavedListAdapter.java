package com.example.devchallenge11halffinal.devchallenge11halffinal.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.devchallenge11halffinal.devchallenge11halffinal.R;
import com.example.devchallenge11halffinal.devchallenge11halffinal.holders.SaveListHolder;
import com.example.devchallenge11halffinal.devchallenge11halffinal.listeners.SavedListFileClick;
import com.example.devchallenge11halffinal.devchallenge11halffinal.storage.SaveToFile;

import java.io.File;

public class SavedListAdapter extends RecyclerView.Adapter<SaveListHolder> {
    private File[] mDataset;
    private SavedListFileClick mSavedListFileClick;

    public void setSavedListFileClick(@NonNull SavedListFileClick savedListFileClick) {
        mSavedListFileClick = savedListFileClick;
    }

    public void setDataset(@NonNull File[] dataset) {
        this.mDataset = dataset;
    }

    @Override
    public SaveListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.save_list_item, parent, false);
        return new SaveListHolder(view);
    }

    @Override
    public void onBindViewHolder(SaveListHolder holder, final int position) {
        holder.getRootView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSavedListFileClick != null) {
                    mSavedListFileClick.selectFile(mDataset[position]);
                }
            }
        });
        holder.setGameName(mDataset[position].getName().replace(SaveToFile.FILE_PREFIX, ""));
    }

    @Override
    public int getItemCount() {
        return mDataset == null ? 0 : mDataset.length;
    }
}
