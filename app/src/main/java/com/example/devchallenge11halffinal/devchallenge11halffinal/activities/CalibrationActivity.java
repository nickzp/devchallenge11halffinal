package com.example.devchallenge11halffinal.devchallenge11halffinal.activities;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.devchallenge11halffinal.devchallenge11halffinal.R;
import com.example.devchallenge11halffinal.devchallenge11halffinal.listeners.SensorAngleListener;
import com.example.devchallenge11halffinal.devchallenge11halffinal.sensors.SensorHelper;
import com.example.devchallenge11halffinal.devchallenge11halffinal.storage.preferences.AppPreferences;

public class CalibrationActivity extends SensorActivity {
    private TextView mCalibrationYSavedSettings;
    private TextView mCalibrationYSensor;
    private TextView mCalibrationXSavedSettings;
    private TextView mCalibrationXSensor;
    private AppPreferences mAppPreferences;
    private float mXAngle;
    private float mYAngle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAppPreferences = new AppPreferences(this.getApplicationContext());

        setContentView(R.layout.activity_calibration);
        initViews();
    }

    private void initViews() {
        Button mSaveAllButton = (Button) findViewById(R.id.saveAllButton);
        Button mResetButton = (Button) findViewById(R.id.resetButton);
        Button mSaveY = (Button) findViewById(R.id.saveY);
        mCalibrationYSavedSettings = (TextView) findViewById(R.id.calibrationYSavedSettings);
        mCalibrationYSensor = (TextView) findViewById(R.id.calibrationYSensor);
        Button mSaveX = (Button) findViewById(R.id.saveX);
        mCalibrationXSavedSettings = (TextView) findViewById(R.id.calibrationXSavedSettings);
        mCalibrationXSensor = (TextView) findViewById(R.id.calibrationXSensor);
        mResetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAppPreferences.setFilterAngleX(SensorHelper.FILTER_ANGLE);
                mAppPreferences.setFilterAngleY(SensorHelper.FILTER_ANGLE);
                updateCalibrationYSavedSettings();
                updateCalibrationXSavedSettings();
            }
        });
        updateCalibrationYSavedSettings();
        updateCalibrationXSavedSettings();
        setSensorAngleListener(new SensorAngleListener() {
            @Override
            public void newSensorAngel(float xAngle, float yAngle) {
                mXAngle = xAngle;
                mYAngle = yAngle;
                updateSensorDate();
            }
        });
        mSaveY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveY();
                updateCalibrationYSavedSettings();
            }
        });
        mSaveX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveX();
                updateCalibrationXSavedSettings();
            }
        });
        mSaveAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveX();
                saveY();
                updateCalibrationXSavedSettings();
                updateCalibrationYSavedSettings();
            }
        });
    }

    private void saveX() {
        mAppPreferences.setFilterAngleX(mXAngle);
    }

    private void saveY() {
        mAppPreferences.setFilterAngleY(mYAngle);
    }

    private void updateSensorDate() {
        mCalibrationYSensor.setText(String.format(getResources().getString(R.string.calibration_date_from_sensor), mYAngle));

        mCalibrationXSensor.setText(String.format(getResources().getString(R.string.calibration_date_from_sensor), mXAngle));
    }

    private void updateCalibrationXSavedSettings() {
        mCalibrationXSavedSettings.setText(String.format(getResources().getString(R.string.calibration_current_setting), mAppPreferences.getFilterAngleX()));
    }

    private void updateCalibrationYSavedSettings() {
        mCalibrationYSavedSettings.setText(String.format(getResources().getString(R.string.calibration_current_setting), mAppPreferences.getFilterAngleY()));
    }
}
