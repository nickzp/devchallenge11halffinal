package com.example.devchallenge11halffinal.devchallenge11halffinal.dialogFragments;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.RadioButton;

import com.example.devchallenge11halffinal.devchallenge11halffinal.R;
import com.example.devchallenge11halffinal.devchallenge11halffinal.enums.GameModeType;
import com.example.devchallenge11halffinal.devchallenge11halffinal.listeners.DialogButtonListener;
import com.example.devchallenge11halffinal.devchallenge11halffinal.storage.preferences.AppPreferences;

public class GameModeDialogFragment extends DialogFragment {
    private AppPreferences mAppPreferences;
    private DialogButtonListener mDialogButtonListener;

    public void setDialogButtonListener(@NonNull DialogButtonListener dialogButtonListener) {
        mDialogButtonListener = dialogButtonListener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        mAppPreferences = new AppPreferences(getActivity());
        View dialogView = getActivity().getLayoutInflater().inflate(R.layout.game_mode_dialog_fragment, null);
        final RadioButton normalRadioButton = (RadioButton) dialogView.findViewById(R.id.normalRadioButton);
        final RadioButton randomRadioButton = (RadioButton) dialogView.findViewById(R.id.randomRadioButton);
        if (mAppPreferences.getGameModeType() == GameModeType.NORMAL) {
            normalRadioButton.setChecked(true);
        } else {
            randomRadioButton.setChecked(true);
        }
        dialogBuilder.setView(dialogView);
        dialogBuilder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mAppPreferences.setGameModeType(randomRadioButton.isChecked() ? GameModeType.RANDOM : GameModeType.NORMAL);
                if (mDialogButtonListener != null) {
                    mDialogButtonListener.OnPositiveButtonClick();
                }

            }
        });
        dialogBuilder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        return dialogBuilder.create();
    }
}
