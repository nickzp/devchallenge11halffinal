package com.example.devchallenge11halffinal.devchallenge11halffinal.dialogFragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.devchallenge11halffinal.devchallenge11halffinal.R;
import com.example.devchallenge11halffinal.devchallenge11halffinal.adapters.SavedListAdapter;
import com.example.devchallenge11halffinal.devchallenge11halffinal.listeners.SavedListFileClick;

import java.io.File;

public class SavedListDialogFragment extends DialogFragment {
    private SavedListFileClick mSavedListFileClick;

    public void setSavedListFileClick(@NonNull SavedListFileClick savedListFileClick) {
        mSavedListFileClick = savedListFileClick;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        View dialogView = getActivity().getLayoutInflater().inflate(R.layout.save_list_dialog_fragment, null);
        File[] filesArray = getActivity().getCacheDir().listFiles();
        dialogBuilder.setView(dialogView);

        if (filesArray == null || filesArray.length == 0) {
            dialogView.findViewById(R.id.emptyTextView).setVisibility(View.VISIBLE);
        } else {
            dialogView.findViewById(R.id.emptyTextView).setVisibility(View.GONE);
            RecyclerView list = (RecyclerView) dialogView.findViewById(R.id.list);
            list.setHasFixedSize(true);
            list.setLayoutManager(new LinearLayoutManager(getActivity()));
            SavedListAdapter mAdapter = new SavedListAdapter();
            mAdapter.setDataset(filesArray);
            mAdapter.setSavedListFileClick(mSavedListFileClick);
            list.setAdapter(mAdapter);
        }

        dialogBuilder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        return dialogBuilder.create();
    }

}
