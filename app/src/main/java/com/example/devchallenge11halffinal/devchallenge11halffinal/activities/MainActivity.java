package com.example.devchallenge11halffinal.devchallenge11halffinal.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.devchallenge11halffinal.devchallenge11halffinal.R;
import com.example.devchallenge11halffinal.devchallenge11halffinal.dialogFragments.GameModeDialogFragment;
import com.example.devchallenge11halffinal.devchallenge11halffinal.dialogFragments.LabyrinthSizeDialogFragment;
import com.example.devchallenge11halffinal.devchallenge11halffinal.dialogFragments.SavedListDialogFragment;
import com.example.devchallenge11halffinal.devchallenge11halffinal.enums.LabyrinthPointType;
import com.example.devchallenge11halffinal.devchallenge11halffinal.labyrinth.LabyrinthCalculation;
import com.example.devchallenge11halffinal.devchallenge11halffinal.labyrinth.LabyrinthGameListener;
import com.example.devchallenge11halffinal.devchallenge11halffinal.labyrinth.LabyrinthMatrix;
import com.example.devchallenge11halffinal.devchallenge11halffinal.labyrinth.LabyrinthPoint;
import com.example.devchallenge11halffinal.devchallenge11halffinal.listeners.BallAnimationListener;
import com.example.devchallenge11halffinal.devchallenge11halffinal.listeners.DialogButtonListener;
import com.example.devchallenge11halffinal.devchallenge11halffinal.listeners.SavedListFileClick;
import com.example.devchallenge11halffinal.devchallenge11halffinal.listeners.SensorListener;
import com.example.devchallenge11halffinal.devchallenge11halffinal.storage.ReadFromFile;
import com.example.devchallenge11halffinal.devchallenge11halffinal.storage.SaveToFile;
import com.example.devchallenge11halffinal.devchallenge11halffinal.storage.preferences.AppPreferences;
import com.example.devchallenge11halffinal.devchallenge11halffinal.utils.DateUtils;
import com.example.devchallenge11halffinal.devchallenge11halffinal.utils.DisplayUtils;
import com.example.devchallenge11halffinal.devchallenge11halffinal.utils.LabyrinthUtils;
import com.example.devchallenge11halffinal.devchallenge11halffinal.views.LabyrinthView;

import java.io.File;

public class MainActivity extends SensorActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private LabyrinthView mLabyrinthView;

    private Button upButton;
    private Button bottomButton;
    private Button leftButton;
    private Button rightButton;
    private Button mNewGameButton;
    private Button mRecreateGameButton;
    private Button mPlayPauseButton;
    private Button mSaveGame;
    private Button mToHoleButton;
    private ConstraintLayout mLabyrinthConstraintLayout;
    private ConstraintLayout mNewGameConstraintLayout;

    private View mBallView;
    private AppPreferences mAppPreferences;
    private NavigationView mNavigationView;
    private boolean isPauseMode;
    private LabyrinthMatrix mLabyrinthMatrix;
    private SavedListDialogFragment mSavedListDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAppPreferences = new AppPreferences(this.getApplicationContext());
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        initViews();
        setListeners();
    }

    private void updatePauseMode(boolean pauseStatus) {
        isPauseMode = pauseStatus;
        mPlayPauseButton.setText(isPauseMode ? R.string.main_play : R.string.main_pause);
        upButton.setEnabled(!isPauseMode);
        bottomButton.setEnabled(!isPauseMode);
        leftButton.setEnabled(!isPauseMode);
        rightButton.setEnabled(!isPauseMode);
    }

    private void enableGameStatusButton(boolean status) {
        mToHoleButton.setEnabled(status);
        mSaveGame.setEnabled(status);
        mPlayPauseButton.setEnabled(status);
        mNewGameButton.setEnabled(status);
        mRecreateGameButton.setEnabled(status);
    }

    private void setListeners() {
        final Context context = this;
        mToHoleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatePauseMode(true);
                enableGameStatusButton(false);
                mLabyrinthView.moveToHole();
            }
        });
        mSaveGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String filename = SaveToFile.write(context, mLabyrinthMatrix, DateUtils.returnNow());
                int toastString;
                if (TextUtils.isEmpty(filename)) {
                    toastString = R.string.main_game_saved_with_error;
                } else {
                    toastString = R.string.main_game_saved_successfully;
                    mAppPreferences.setPoint(filename, mLabyrinthView.getHolePoint(), LabyrinthPointType.HOLE);
                    mAppPreferences.setPoint(filename, mLabyrinthView.getBallPoint(), LabyrinthPointType.BALL);

                }
                Toast.makeText(context, toastString, Toast.LENGTH_LONG).show();
            }
        });
        mPlayPauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updatePauseMode(!isPauseMode);
            }
        });
        mNavigationView.setNavigationItemSelectedListener(this);
        createAndLoadLabyrinth();
        setSensorListener(new SensorListener() {
            @Override
            public void rotatedToTop() {
                moveUp();
            }

            @Override
            public void rotatedToBottom() {
                moveBottom();
            }

            @Override
            public void rotatedToLeft() {
                moveLeft();
            }

            @Override
            public void rotatedToRight() {
                moveRight();
            }
        });

        upButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveUp();
            }
        });

        bottomButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveBottom();
            }
        });

        leftButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveLeft();
            }
        });

        rightButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveRight();
            }
        });

        mNewGameButton.setOnClickListener(newGameClickListener());
        mRecreateGameButton.setOnClickListener(newGameClickListener());
        mLabyrinthView.setBallMoveListener(new BallAnimationListener() {
            @Override
            public void moveAnimationEnd() {
                resetTimer();
            }
        });
        mLabyrinthView.setLabyrinthGameListener(new LabyrinthGameListener() {
            @Override
            public void gameOver() {
                updatePauseMode(false);
                enableGameStatusButton(true);
                mBallView.invalidate();
                mLabyrinthConstraintLayout.setVisibility(View.GONE);
                mNewGameConstraintLayout.setVisibility(View.VISIBLE);
            }
        });
    }

    private void moveRight() {
        if (!isPauseMode) {
            mLabyrinthView.moveRight();
        }
    }

    private void moveLeft() {
        if (!isPauseMode) {
            mLabyrinthView.moveLeft();
        }
    }

    private void moveUp() {
        if (!isPauseMode) {
            mLabyrinthView.moveUp();
        }
    }

    private void moveBottom() {
        if (!isPauseMode) {
            mLabyrinthView.moveBottom();
        }
    }

    private void initViews() {
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        upButton = (Button) findViewById(R.id.upButton);
        bottomButton = (Button) findViewById(R.id.bottomButton);
        leftButton = (Button) findViewById(R.id.leftButton);
        rightButton = (Button) findViewById(R.id.rightButton);
        mNewGameConstraintLayout = (ConstraintLayout) findViewById(R.id.newGameConstraintLayout);
        mNewGameButton = (Button) findViewById(R.id.newGameButton);
        mRecreateGameButton = (Button) findViewById(R.id.recreateGameButton);
        mPlayPauseButton = (Button) findViewById(R.id.playPauseButton);
        mLabyrinthConstraintLayout = (ConstraintLayout) findViewById(R.id.labyrinthConstraintLayout);
        mSaveGame = (Button) findViewById(R.id.saveButton);
        mBallView = findViewById(R.id.ballView);
        mToHoleButton = (Button) findViewById(R.id.moveToHoleButton);
    }

    private View.OnClickListener newGameClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNewGameConstraintLayout.setVisibility(View.GONE);
                createAndLoadLabyrinth();
                mLabyrinthConstraintLayout.setVisibility(View.VISIBLE);
            }
        };
    }

    private void createAndLoadLabyrinth() {
        loadLabyrinth(createNewLabyrinth());
    }

    private LabyrinthMatrix createNewLabyrinth() {
        LabyrinthCalculation labyrinthCalculation = new LabyrinthCalculation(DisplayUtils.getWidthInPixels(this));
        return new LabyrinthMatrix(labyrinthCalculation.countCellCorrector(mAppPreferences.getCellCountX()), labyrinthCalculation.countCellCorrector(mAppPreferences.getCellCountY()));
    }

    private void loadLabyrinth(@NonNull LabyrinthMatrix matrix) {
        LabyrinthPoint holePoint = LabyrinthUtils.getHolePoint(matrix, mAppPreferences);
        LabyrinthPoint ballPoint = LabyrinthUtils.getBallPoint(matrix, mAppPreferences, holePoint);
        loadLabyrinth(matrix, holePoint, ballPoint);
    }

    private void loadLabyrinth(@NonNull LabyrinthMatrix matrix, @NonNull LabyrinthPoint holePoint, @NonNull LabyrinthPoint ballPoint) {
        mLabyrinthMatrix = matrix;
        mLabyrinthView = (LabyrinthView) findViewById(R.id.LabyrinthView);
        mLabyrinthView.setHolePoint(holePoint);
        mLabyrinthView.setLabyrinthMatrix(matrix);
        mLabyrinthView.setBallView(mBallView, ballPoint);
        mLabyrinthView.invalidate();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.labyrinth_size_dialog) {
            LabyrinthSizeDialogFragment dialogSize = new LabyrinthSizeDialogFragment();
            dialogSize.show(getSupportFragmentManager(), LabyrinthSizeDialogFragment.class.getName());
            dialogSize.setDialogButtonListener(new DialogButtonListener() {
                @Override
                public void OnPositiveButtonClick() {
                    createAndLoadLabyrinth();
                }
            });
        } else if (id == R.id.game_mode) {
            GameModeDialogFragment gameMode = new GameModeDialogFragment();
            gameMode.show(getSupportFragmentManager(), GameModeDialogFragment.class.getName());
            gameMode.setDialogButtonListener(new DialogButtonListener() {
                @Override
                public void OnPositiveButtonClick() {
                    createAndLoadLabyrinth();
                }
            });
        } else if (id == R.id.calibration) {
            Intent intent = new Intent(this, CalibrationActivity.class);
            startActivity(intent);
        } else if (id == R.id.saveList) {
            mSavedListDialog = new SavedListDialogFragment();
            mSavedListDialog.show(getSupportFragmentManager(), SavedListDialogFragment.class.getName());
            mSavedListDialog.setSavedListFileClick(savedListFileClick());
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private SavedListFileClick savedListFileClick() {
        return new SavedListFileClick() {
            @Override
            public void selectFile(@NonNull File file) {
                String fileName = file.getName();
                loadLabyrinth(ReadFromFile.readLabyrinthMatrix(getBaseContext(), fileName),
                        mAppPreferences.getPoint(fileName, LabyrinthPointType.HOLE),
                        mAppPreferences.getPoint(fileName, LabyrinthPointType.BALL));
                mSavedListDialog.dismiss();
            }
        };
    }
}
