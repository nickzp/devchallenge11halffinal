package com.example.devchallenge11halffinal.devchallenge11halffinal.enums;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;


public class LabyrinthCellWallsType {

    public static final int ONLY_TOP = 0;
    public static final int ONLY_LEFT = 1;
    public static final int ONLY_RIGHT = 2;
    public static final int ONLY_BOTTOM = 3;
    public static final int MORE_THAN_ONE = 4;
    public static final int ALL_WALLS = 5;

    @IntDef({ONLY_TOP, ONLY_LEFT, ALL_WALLS, ONLY_RIGHT, ONLY_BOTTOM, MORE_THAN_ONE})

    @Retention(RetentionPolicy.SOURCE)
    public @interface Value {
    }
}
