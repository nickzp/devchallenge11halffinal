package com.example.devchallenge11halffinal.devchallenge11halffinal.enums;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class LabyrinthPointType {
    public static final int BALL = 0;
    public static final int HOLE = 1;

    @IntDef({BALL, HOLE})

    @Retention(RetentionPolicy.SOURCE)
    public @interface Value {
    }
}


