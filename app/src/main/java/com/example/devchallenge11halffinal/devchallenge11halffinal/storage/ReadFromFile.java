package com.example.devchallenge11halffinal.devchallenge11halffinal.storage;


import android.content.Context;
import android.support.annotation.NonNull;

import com.example.devchallenge11halffinal.devchallenge11halffinal.labyrinth.LabyrinthMatrix;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.StreamCorruptedException;

public class ReadFromFile {
    public static LabyrinthMatrix readLabyrinthMatrix(@NonNull Context context, @NonNull String fileName) {
        ObjectInputStream input;
        LabyrinthMatrix labyrinthMatrix;
        try {
            input = new ObjectInputStream(new FileInputStream(new File(new File(context.getCacheDir(), "") + File.separator + fileName)));
            labyrinthMatrix = (LabyrinthMatrix) input.readObject();
            input.close();
            return labyrinthMatrix;
        } catch (StreamCorruptedException | ClassNotFoundException | FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
